#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socketserver, sys, os, threading, time, json, copy
from time import ctime
import txrx as t

snapshot_period = 60
unix_domain = './uds_socket'

devices = {}
dfm_logs = {}
class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        cur = threading.current_thread()
        print('[%s][%s]' % (ctime(), cur.name), end = '')

        payload = t.receive(self.request) 
        segment = payload.split('!#')       
        opc = segment[1]
        data = segment[2]

        global devices
        global dfm_logs
        segment = data.split('@$')
        if opc == 'C': # add a advice
            d_id = segment[1]
            device_inf = json.loads(segment[2])
            devices[d_id] = copy.deepcopy(device_inf)
            t.transmit(self.request, 'OK')
            #print('\n', devices)
 
        elif opc == 'R': # read a specific device
            d_id = segment[1]
            if d_id in devices:
                device = json.dumps(devices.get(d_id))
            else:
                device = 'NOT FOUND'
            t.transmit(self.request, device)

        elif opc == 'U': # update the df value of a device
            d_id = segment[1]
            df_name = segment[2]
            df_value = json.loads(segment[3])
            try:
                df_value = json.loads(df_value)
            except:
                pass
            if (d_id in devices) and (df_name in devices[d_id]):
                devices[d_id][df_name] = copy.deepcopy(df_value)
                t.transmit(self.request, 'OK')
            else:
                t.transmit(self.request, 'NOT FOUND')
            #print('\n', devices)

        elif opc == 'D': # del a specific device
            d_id = segment[1]            
            try:
                devices.pop(d_id)
            except:
                pass
            t.transmit(self.request, 'OK')
            #print('\n', devices)

        elif opc == 'A': # read all devices
            all_devices = json.dumps(devices)
            t.transmit(self.request, all_devices)

        elif opc == 'RF': # read the df value of a device
            d_id = segment[1]
            df_name = segment[2]
            df_data = json.dumps(devices[d_id].get(df_name))
            t.transmit(self.request, df_data)

        elif opc == 'M':  #update min_max
            d_id = segment[1]
            df_name = segment[2]
            min_max_value = json.loads(segment[3])
            if (d_id in devices) and (df_name in devices[d_id]['profile']['min_max']):
                devices[d_id]['profile']['min_max'][df_name] = copy.deepcopy(min_max_value)
                t.transmit(self.request, 'OK')
            else:
                t.transmit(self.request, 'NOT FOUND')
            #print('\n', devices)

        elif opc == 'dfmC': # add an new dfm item
            key = tuple(json.loads(segment[1]))
            dfm_inf = json.loads(segment[2])
            dfm_logs[key] = copy.deepcopy(dfm_inf)
            t.transmit(self.request, 'OK')
            #print('\n', dfm_logs)

        elif opc == 'dfmR': # read a specific dfm item 
            key = tuple(json.loads(segment[1]))
            if key in dfm_logs:
                item = json.dumps(dfm_logs.get(key))
            else:
                item = 'NOT FOUND'
            t.transmit(self.request, item)

        elif opc == 'dfmU': # update the value of an item
            key = tuple(json.loads(segment[1]))
            stage = segment[2]
            minmax_list = json.loads(segment[3])
            if (key in dfm_logs) and (stage in dfm_logs[key]):
                dfm_logs[key][stage]['min_max'] = copy.deepcopy(minmax_list)
                t.transmit(self.request, 'OK')
            else:
                 t.transmit(self.request, 'NOT FOUND')
            #print('\n', dfm_logs)

        elif opc == 'dfmD': # del a dfm item
            key = tuple(json.loads(segment[1]))
            try:
                dfm_logs.pop(key)
            except:
                pass
            t.transmit(self.request, 'OK')
            #print('\n', dfm_logs)

        elif opc == 'dfmDA': # del all dfm items
            dfm_logs = {}
            t.transmit(self.request, 'OK')
            #print('\n', dfm_logs)

        else:
            print('Error OP code. Pass.')

        self.request.close()


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.UnixStreamServer):
    daemon_threads = True
    allow_reuse_address = True


def devices_snapshot():
    snapshot = open('csm_state_snapshot', 'w')
    snapshot.write(str(ctime())+'\n')
    snapshot.write(str(devices))
    snapshot.close()


def snapshot(backoff_time, snapshot_period):    # Periodically stores CSM state...
    time.sleep(backoff_time)                    # #Wait backoff time for ESM state restoring
    print('\nStarting to periodically store CSM state every {} seconds.'.format(snapshot_period))
    while True:
        devices_snapshot()
        time.sleep(snapshot_period)


if __name__ == '__main__':

    snapshot_thx = threading.Thread(target=snapshot, args=(120, snapshot_period)) 
    snapshot_thx.daemon = True
    snapshot_thx.start()

    server_addr = unix_domain
    try:
        os.unlink(server_addr)
    except OSError:
        pass

    server = ThreadedTCPServer(server_addr, ThreadedTCPRequestHandler)
    addr = server.server_address
    print('Server start at: %s' % addr)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        sys.exit(0)
