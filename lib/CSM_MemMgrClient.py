import sys, socket, time, json
import CSM_MemMgrServer
import txrx as t

server_address = CSM_MemMgrServer.unix_domain

def connect(server_addr):
    uds_sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    #print ('connecting to', server_addr)
    try:
        uds_sock.connect(server_addr)
    except socket.error as msg:
        print (msg)
        sys.exit(1)
    return uds_sock

def pack(OP_Code, d_id=None, df_name=None, data=None):
    if df_name: 
        payload = '{}{}{}'.format(df_name,'@$',json.dumps(data))
    else:
        payload = data
    payload = '{}{}{}{}'.format('@$', d_id, '@$', payload)
    payload = '{}{}{}{}'.format('!#', OP_Code, '!#', payload)
    return payload

def create(d_id, device_dir):
    sock = connect(server_address)
    try:
        OP_Code = 'C'
        data = json.dumps(device_dir)
        packet = pack(OP_Code, d_id, None, data)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        return result

def read_df(d_id, df_name):
    sock = connect(server_address)
    try:
        OP_Code = 'RF'    
        data = df_name
        packet = pack(OP_Code, d_id, None, data)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        df_data = json.loads(result)
        return df_data 

def read_device(d_id):
    sock = connect(server_address)
    try:
        OP_Code = 'R'
        packet = pack(OP_Code, d_id)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        if result != 'NOT FOUND':
            device_inf = json.loads(result)
            return device_inf
        else:
            return None
        
def update(d_id, df_name, df_data):
    sock = connect(server_address)
    try:
        OP_Code = 'U'
        data = json.dumps(df_data)
        packet = pack(OP_Code, d_id, df_name, data)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        return result

def update_minmax(d_id, df_name, minmax_value):
    sock = connect(server_address)
    try:
        OP_Code = 'M'
        packet = pack(OP_Code, d_id, df_name, minmax_value)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        return result

def delete(d_id):
    sock = connect(server_address)
    try:
        OP_Code = 'D'
        packet = pack(OP_Code, d_id)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        return result

def all_devices():
    sock = connect(server_address)
    try:
        OP_Code = 'A'
        packet = pack(OP_Code)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        all_device_inf = json.loads(result)
        return all_device_inf 


def DFMcreate(keyTuple, dfm_inf):
    sock = connect(server_address)
    try:
        OP_Code = 'dfmC'
        key = json.dumps(keyTuple)
        data = json.dumps(dfm_inf)
        packet = pack(OP_Code, key, None, data)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        return result

def DFMread(keyTuple):
    sock = connect(server_address)
    try:
        OP_Code = 'dfmR'
        key = json.dumps(keyTuple)
        packet = pack(OP_Code, key)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        if result != 'NOT FOUND':
            dfm_inf = json.loads(result)
            return dfm_inf
        else:
            return None

def DFMupdate(keyTuple, stage, min_max):
    sock = connect(server_address)
    try:
        OP_Code = 'dfmU'
        key = json.dumps(keyTuple)
        data = json.dumps(min_max)
        packet = pack(OP_Code, key, stage, data)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        return result

def DFMdel(keyTuple):
    sock = connect(server_address)
    try:
        OP_Code = 'dfmD'
        key = json.dumps(keyTuple)
        packet = pack(OP_Code, key)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:
        print (msg)
    finally:
        sock.close()
        return result

def DFMdelAll():
    sock = connect(server_address)
    try:
        OP_Code = 'dfmDA'
        packet = pack(OP_Code)
        t.transmit(sock, packet)
        result = t.receive(sock)
    except Exception as msg:                                                                                                                 print (msg)
    finally:
        sock.close()
        return result
    
        

if __name__ == '__main__':

    a={}
    a["profile"]={"d_name":"S","df_list":["Flow","CO2-O"],"dm_name":"W","is_sim":'false',"min_max":{"Flow":[[9.2,9.7]],"CO2-O":[[4.0,5.7]],},"u_name":"y"}
    a["Flow"]=[['2022-05-23 13:14:45.330751', [4.7]], ['2022-05-23 13:14:20.965909', [4.8]]]
    a["CO2-O"]=[['2022-05-23 13:14:45.078040', [4.3]], ['2022-05-23 13:14:20.627482', [4.7]]]
    a["__Ctl_I__"]=[]
    a["__Ctl_O__"]=[]
    a["password"]= '2825b94b-89a0-4cf6-8fe9-bc593b7696f5'


    d_id = 'SimDev64'    

    #d_id = 'cccA'
    #r = create(d_id, a)
    #r = read(d_id, 'Flow')
    #r = read_device(d_id)
    #r = update(d_id, 'Flow', a['CO2-O'])
    #r = update_minmax(d_id, 'Flow', a['profile']['min_max']['CO2-O'])
    #r = delete(d_id)
    r=all_devices()
    #print(type(r))
    #print('\nresult:\n', r)

    dfm={}
    dfm[('158', '304')]={'input': {'samples': [['2022-05-24 20:38:51.559684', [0.38214450656612586]], ['2022-05-24 20:38:50.569768', [0.3477422375454914]], ['2022-05-24 20:38:49.490241', [0.7872059884645329]]], 'min_max': [[0.019211054966557506, 0.9672895006778299]]}, 'type': {'samples': [['2022-05-24 20:38:51.563318', [0.38214450656612586]], ['2022-05-24 20:38:50.573308', [0.3477422375454914]], ['2022-05-24 20:38:49.493832', [0.7872059884645329]]], 'min_max': [[0.019211054966557506, 0.9672895006778299]]}, 'function': {'samples': [['2022-05-24 20:38:51.566891', [0.38214450656612586]], ['2022-05-24 20:38:50.583238', [0.3477422375454914]], ['2022-05-24 20:38:49.497393', [0.7872059884645329]]], 'min_max': [[6, 9]]}}


    keyT = ('22', '304')
    #r = DFMcreate(keyT, dfm[('158', '304')])
    #r = DFMread(keyT)
    #r = DFMupdate(keyT, 'type', dfm[('158', '304')]['function']['min_max'])
    #r = DFMdel(keyT)
    #r = DFMdelAll()

    print('\nresult:\n', r)
