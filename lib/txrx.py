#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def receive(socket_obj):
    msg=b''
    msg_len=99999999
    amount_received=0
    while True:
        if amount_received >= msg_len: break
        raw = socket_obj.recv(2048)
        if len(raw) == 0: break

        rec = str(raw).split('*~')
        if len(rec)>2: msg_len=int(rec[1])
        amount_received += len(raw)

        if len(raw)>30:
            print('recv: {}{}'.format((raw.decode())[:40], '... (omitted)'))
        else:
            print('recv: {}'.format(raw.decode()))
        msg+=raw
    payload = (msg.decode()).split('*~')[2]
    return payload


def transmit(socket_obj, msg):
    n_digit = 8
    r_msg = msg
    msg_len = 4+len(r_msg)+n_digit #len(str(6+len(r_msg)))
    outdata = '{}{}{}{}'.format('*~', str(msg_len).zfill(n_digit), '*~', r_msg)
    socket_obj.sendall(outdata.encode())

